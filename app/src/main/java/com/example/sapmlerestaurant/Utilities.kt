package com.example.sapmlerestaurant

import android.content.Context
import org.json.JSONArray
import java.io.IOException

/**
 * reading json from file in the raw directory
 * @return a jsonArray based on the file
 */
fun readJsonFile(context : Context) : JSONArray{

    var json : String? = null

    try {
        val input = context.resources.openRawResource(R.raw.menu)
        json = input.bufferedReader().use { it.readText() }
    }
    catch (e : IOException){
        e.printStackTrace()
    }

    return JSONArray(json)
}

/**
 * this function calculate a float number for rating stars
 * this is not a correct logical number just for generating number
 */
fun calculateRatings(rate : Int) : Float = 1000 / rate.toFloat()