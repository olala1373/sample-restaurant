package com.example.sapmlerestaurant

import android.view.ViewManager
import android.widget.RatingBar
import androidx.appcompat.view.ContextThemeWrapper
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.jetbrains.anko.custom.ankoView

/**
 * extension function to achieve bottomNavigationView in kotlin
 */
inline fun ViewManager.bottomNavigation(init: BottomNavigationView.() -> Unit = {}) =
    ankoView({ BottomNavigationView(it) }, theme = 0, init = init)

/**
 * extension function for generating a ratingBar with custom style
 */
 inline fun ViewManager.ratingBarStyle(styleRes: Int = 0, init: RatingBar.() -> Unit): RatingBar {
    return ankoView({ if (styleRes == 0) RatingBar(it)
    else RatingBar(ContextThemeWrapper(it, styleRes), null, 0 ) }
    , R.style.Base_Widget_AppCompat_RatingBar_Small) {
        init()
    }
}