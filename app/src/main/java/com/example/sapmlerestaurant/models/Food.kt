package com.example.sapmlerestaurant.models

data class Food(var image : String , var title : String , var subtitle : String , var time : String , var rate : Int)