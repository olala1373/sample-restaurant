package com.example.sapmlerestaurant.models

data class Meal(var image : String , var title : String , var subtitle : String , var time : String , var rate : Int)