package com.example.sapmlerestaurant

import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import com.example.sapmlerestaurant.fragments.RestaurantFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.jetbrains.anko.*

class MainActivityUI : AnkoComponent<MainActivity> {

    companion object mainIds {
        val bottom_navigation = View.generateViewId()
        val fragment_container = View.generateViewId()
        val toolbar = View.generateViewId()
    }
    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        relativeLayout {
            backgroundColorResource = R.color.gray_200
            myToolbar().lparams {
                width = matchParent
                height = dip(60)
                alignParentTop()
            }

            frameLayout {
                id = fragment_container
            }.lparams {
                width = matchParent
                height = matchParent
                below(toolbar)
                above(bottom_navigation)
                topMargin = dip(4)
            }

            owner.bottomNavigationView = bottomNavigation {
                inflateMenu(R.menu.navigation)
                id = bottom_navigation
                backgroundColorResource = R.color.gray_300
            }.lparams {
                alignParentBottom()
                width = matchParent
                height = dip(60)
            }

        }
    }

    private fun ViewGroup.myToolbar(): View {
        return linearLayout {
            id = toolbar
            backgroundColor = Color.WHITE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val shadow  = 4f
                elevation = shadow
            }
            gravity = Gravity.CENTER_VERTICAL
            editText {
                hint = "Restaurants & dishes"
                gravity = Gravity.CENTER_VERTICAL
                backgroundDrawable = ContextCompat.getDrawable(context, R.drawable.edittext_background)
                padding = dip(8)
                compoundDrawablePadding = dip(8)
                val drawable = context.resources.getDrawable(R.drawable.ic_search)
                drawable.setBounds(0,0,48,48)
                setCompoundDrawables(
                    drawable
                    , null , null, null
                )
                gravity = Gravity.CENTER_VERTICAL

            }.lparams {
                width = dip(220)
                height = dip(40)
                marginStart = dip(8)
            }

            relativeLayout {
                val cuisinesId = 1
                imageView(R.drawable.ic_restaurant_blue) {
                    id = cuisinesId
                }.lparams {
                    width = dip(20)
                    height = dip(20)
                    centerHorizontally()
                }
                textView {
                    text = context.getString(R.string.cuisine)
                    textColorResource = R.color.custom_blue
                    textSize = 14f
                }.lparams {
                    width = wrapContent
                    height = wrapContent
                    centerHorizontally()
                    below(cuisinesId)
                }
            }.lparams {
                width = wrapContent
                height = wrapContent
                marginStart = dip(16)
                marginEnd = dip(8)
            }

            imageView {
                backgroundColorResource = R.color.gray_300
            }.lparams{
                width = dip(1)
                height = matchParent
                topMargin = dip(20)
                bottomMargin = dip(20)
            }

            relativeLayout {
                val cuisinesId = 1
                imageView(R.drawable.ic_settings) {
                    id = cuisinesId
                }.lparams {
                    width = dip(20)
                    height = dip(20)
                    centerHorizontally()
                }
                textView {
                    text = context.getString(R.string.refine)
                    textColorResource = R.color.custom_blue
                }.lparams {
                    width = wrapContent
                    height = wrapContent
                    centerHorizontally()
                    below(cuisinesId)
                }
            }.lparams {
                width = wrapContent
                height = wrapContent
                marginStart = dip(8)
            }

        }
    }

}