package com.example.sapmlerestaurant.adapters


import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sapmlerestaurant.adapters.ui.FoodAdapterUI
import com.example.sapmlerestaurant.calculateRatings
import com.example.sapmlerestaurant.models.Food
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext


class FoodAdapter(var foods : List<Food>) : RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        return FoodViewHolder(FoodAdapterUI().createView(AnkoContext.create(parent.context , parent)))
    }

    override fun getItemCount(): Int = foods.size

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
       holder.setData(foods[position])
    }


    inner class FoodViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var image : ImageView = itemView.findViewById(FoodAdapterUI.foodImage)
        var title : TextView = itemView.findViewById(FoodAdapterUI.title)
        var subtitle : TextView = itemView.findViewById(FoodAdapterUI.subtitle)
        var time : TextView = itemView.findViewById(FoodAdapterUI.deliveryTime)
        var rate : TextView = itemView.findViewById(FoodAdapterUI.ratingCount)
        var rateStar : RatingBar = itemView.findViewById(FoodAdapterUI.ratingBar)

        fun setData( food : Food){
            Picasso.get().load(food.image).centerCrop().resize(120 , 120).into(image)
            title.text = food.title
            subtitle.text = food.subtitle
            time.text = food.time
            rate.text = "${food.rate} Ratings"
            rateStar.rating = calculateRatings(food.rate)
        }
    }
}