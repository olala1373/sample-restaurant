package com.example.sapmlerestaurant.adapters

import android.media.Image
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sapmlerestaurant.adapters.ui.MealAdapterUI
import com.example.sapmlerestaurant.calculateRatings
import com.example.sapmlerestaurant.models.Meal
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext
import org.w3c.dom.Text

class MealAdapter(var meals : List<Meal>) : RecyclerView.Adapter<MealAdapter.MealViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        return MealViewHolder(MealAdapterUI().createView(AnkoContext.create(parent.context , parent)))
    }

    override fun getItemCount(): Int = meals.size

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) {
        holder.setData(meals[position])
    }


    inner class MealViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var image : ImageView = itemView.findViewById(MealAdapterUI.headerImage)
        var title : TextView = itemView.findViewById(MealAdapterUI.titleText)
        var subtitle : TextView = itemView.findViewById(MealAdapterUI.subtitleText)
        var time : TextView = itemView.findViewById(MealAdapterUI.timeText)
        var rate : TextView = itemView.findViewById(MealAdapterUI.ratingCount)
        var rateStar : RatingBar = itemView.findViewById(MealAdapterUI.ratingBar)

        fun setData( meal : Meal){
            Picasso.get().load(meal.image).fit().into(image)
            title.text = meal.title
            subtitle.text = meal.subtitle
            time.text = meal.time
            rate.text = "${meal.rate} Ratings"
            rateStar.rating = calculateRatings(meal.rate)
        }
    }
}