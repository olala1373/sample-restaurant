package com.example.sapmlerestaurant.adapters.ui

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.LayerDrawable
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.example.sapmlerestaurant.R
import com.example.sapmlerestaurant.ratingBarStyle
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class MealAdapterUI : AnkoComponent<ViewGroup> {
    companion object {
        val headerId = View.generateViewId()
        val contentId = View.generateViewId()
        val headerImage = View.generateViewId()
        val freeDeliveryText = View.generateViewId()
        val titleText = View.generateViewId()
        val subtitleText = View.generateViewId()
        val timeText = View.generateViewId()
        val ratingBar = View.generateViewId()
        val ratingCount = View.generateViewId()
    }

    val marginStartDimension = 8

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui){
        cardView {
            lparams{
                width = wrapContent
                height = matchParent
                margin = dip(8)
            }
            radius = 10f

            relativeLayout {

                header().lparams{
                    width = matchParent
                    height = dip(110)
                    alignParentTop()
                }

                content().lparams{
                    width = matchParent
                    height = wrapContent
                    below(headerId)
                }

                footer().lparams{
                    width = dip(270)
                    height = dip(30)
                    alignParentBottom()
                }

            }.lparams{
                width = wrapContent
                height = matchParent
            }
        }
    }

    private fun ViewGroup.header() : View {
        return frameLayout {
            id = headerId
            imageView {
                id = headerImage
            }.lparams {
                width = dip(270)
                height = dip(90)
            }

            textView {
                id = freeDeliveryText
                backgroundDrawable = context.resources.getDrawable(R.drawable.textview_circle_background)
                text = "Free delivery"
                textColor = Color.WHITE
                ellipsize = TextUtils.TruncateAt.END
                maxLines = 2
                textSize = 12f
                gravity = Gravity.CENTER

            }.lparams {
                width = dip(45)
                height = dip(45)
                gravity = Gravity.END + Gravity.BOTTOM
                marginEnd = dip(16)
            }
        }
    }

    fun ViewGroup.content() : View {
        return relativeLayout {
            id = contentId
            textView {
                id = titleText
                textColorResource = R.color.custom_blue
                setTypeface(null , Typeface.BOLD)
                textSize = 16f

            }.lparams {
                width = wrapContent
                height = wrapContent
                marginStart = dip(marginStartDimension)
            }

            textView {
                id = subtitleText
                textColorResource = R.color.gray_400
                textSize = 14f
                maxLines = 1
                ellipsize = TextUtils.TruncateAt.END

            }.lparams {
                width = wrapContent
                height = wrapContent
                marginStart = dip(marginStartDimension)
                below(titleText)
            }

            textView{
                id = timeText
                textColorResource = R.color.gray_400
            }.lparams {
                rightOf(subtitleText)
                below(titleText)
                width = wrapContent
                height = wrapContent
                marginStart = dip(marginStartDimension)
            }


            textView{
                textColorResource = R.color.green
                text = "no min"
            }.lparams {
                rightOf(timeText)
                below(titleText)
                width = wrapContent
                height = wrapContent
                marginStart = dip(marginStartDimension)
            }

            ratingBarStyle(R.style.Base_Widget_AppCompat_RatingBar_Small) {
                id = ratingBar
                numStars = 5
                rating = 4f
                stepSize = 0.1f
                var layer  = this.progressDrawable as LayerDrawable
                layer.getDrawable(2).setColorFilter(Color.parseColor("#F9C931"), PorterDuff.Mode.SRC_ATOP)
                layer.getDrawable(1).setColorFilter(Color.parseColor("#F9C931"), PorterDuff.Mode.SRC_ATOP)
                setIsIndicator(true)

            }.lparams {
                width = wrapContent
                height = wrapContent
                below(subtitleText)
                topMargin = dip(8)
                marginStart = dip(marginStartDimension)
            }
            textView{
                id = ratingCount
                textColorResource = R.color.gray_400

            }.lparams {
                rightOf(ratingBar)
                below(subtitleText)
                width = wrapContent
                height = wrapContent
                marginStart = dip(marginStartDimension)
                topMargin = dip(8)
            }

        }
    }

    private fun ViewGroup.footer() : View {
        return relativeLayout {
            backgroundColorResource = R.color.gray_300
            textView {
                text = "Offer valid on first order only"
                textColorResource = R.color.custom_blue_darker
            }.lparams{
                width = wrapContent
                height = wrapContent
                marginStart = dip(marginStartDimension)
                centerVertically()
            }
        }
    }
}