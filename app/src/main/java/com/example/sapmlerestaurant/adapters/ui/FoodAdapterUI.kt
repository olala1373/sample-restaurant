package com.example.sapmlerestaurant.adapters.ui

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.graphics.drawable.LayerDrawable
import android.media.Image
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.marginBottom
import com.example.sapmlerestaurant.R
import com.example.sapmlerestaurant.ratingBarStyle
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class FoodAdapterUI : AnkoComponent<ViewGroup>{

    companion object{
        val foodImage = View.generateViewId()
        val title = View.generateViewId()
        val subtitle = View.generateViewId()
        val deliveryTime = View.generateViewId()
        val ratingBar = View.generateViewId()
        val ratingCount = View.generateViewId()
    }
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui){
        cardView {
            lparams{
                width = matchParent
                height = wrapContent
                margin = dip(8)
            }
            radius = 10f
            relativeLayout {

                imageView {
                    id = foodImage
                }.lparams {
                    width = dip(120)
                    height = dip(120)
                    alignParentStart()
                }

                textView{
                    id = title
                    textColorResource = R.color.custom_blue
                    setTypeface(null , Typeface.BOLD)
                }.lparams {
                    rightOf(foodImage)
                    topMargin = dip(16)
                    marginStart = dip(8)
                    width = wrapContent
                    height = wrapContent
                }

                textView{
                    id = subtitle
                    textColorResource = R.color.gray_400
                    textSize = 12f
                    text = "subtitle"

                }.lparams{
                    below(title)
                    rightOf(foodImage)
                    marginStart = dip(8)
                    width = wrapContent
                    height = wrapContent
                }

                textView {
                    id = deliveryTime
                    text = "30-45 mins"
                    textColorResource = R.color.gray_700
                    bottomPadding = dip(8)

                }.lparams{
                    rightOf(foodImage)
                    alignParentBottom()
                    marginStart = dip(8)
                    width = wrapContent
                    height = wrapContent
                    bottomMargin = dip(8)
                }



                verticalLayout {
                    ratingBarStyle(R.style.Base_Widget_AppCompat_RatingBar_Small) {
                        id = ratingBar
                        numStars = 5
                        rating = 4f
                        stepSize = 0.1f
                        var layer  = this.progressDrawable as LayerDrawable
                        layer.getDrawable(2).setColorFilter(Color.parseColor("#F9C931"), PorterDuff.Mode.SRC_ATOP)
                        layer.getDrawable(1).setColorFilter(Color.parseColor("#F9C931"), PorterDuff.Mode.SRC_ATOP)
                        setIsIndicator(true)

                    }.lparams {
                        width = wrapContent
                        height = wrapContent
                        marginEnd = dip(8)
                    }

                    textView{
                        id = ratingCount
                        textColorResource = R.color.gray_400
                        text = "rating"

                    }.lparams {
                        width = wrapContent
                        height = wrapContent
                        gravity = Gravity.END
                        marginEnd = dip(8)
                    }

                }.lparams{
                    width = wrapContent
                    height = wrapContent
                    alignParentBottom()
                    alignParentEnd()
                }



            }.lparams {
                width = matchParent
                height = wrapContent
            }


        }
    }


}