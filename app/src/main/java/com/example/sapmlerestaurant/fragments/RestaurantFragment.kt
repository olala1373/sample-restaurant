package com.example.sapmlerestaurant.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.sapmlerestaurant.fragments.ui.HomeUI
import org.jetbrains.anko.AnkoContext

class RestaurantFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        HomeUI().createView(AnkoContext.create(inflater.context, this, false))


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addFoodFragment()
        addMealFragment()
    }

    private fun addFoodFragment(){
        childFragmentManager.beginTransaction()
            .add(HomeUI.foodFragment , FoodFragment())
            .commit()
    }

    private fun addMealFragment(){
        childFragmentManager.beginTransaction()
            .add(HomeUI.mealFragment , MealFragment())
            .commit()
    }
}