package com.example.sapmlerestaurant.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sapmlerestaurant.adapters.FoodAdapter
import com.example.sapmlerestaurant.fragments.ui.FoodUI
import com.example.sapmlerestaurant.models.Food
import com.example.sapmlerestaurant.readJsonFile
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.find
import org.json.JSONArray
import org.json.JSONObject

class FoodFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        FoodUI().createView(AnkoContext.create(inflater.context , this , false))


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val jsonArray : JSONArray = readJsonFile(context!!)

        val adapter = FoodAdapter(extractFoodFromJson(jsonArray))

        var recycler : RecyclerView = find(FoodUI.recyclerId)

        recycler.adapter = adapter
    }


    private fun extractFoodFromJson(jsonArray: JSONArray) : List<Food>{
        val foods = mutableListOf<Food>()
        for (i in 0 until jsonArray.length()){
            val foodObject = jsonArray.getJSONObject(i)
            val image = foodObject.getString("image")
            val name = foodObject.getString("title")
            val cuisineObject = foodObject.getJSONObject(MealFragment.cuisineType)
            val subTitle = cuisineObject.getString("title")
            val delivery = foodObject.getString("delivery").toInt()
            val prepration = foodObject.getString("preparation").toInt()
            val rate = foodObject.getInt("rate")

            val time = delivery + prepration
            foods.add(Food(image , name , subTitle , "$delivery-$time mins" , rate))
        }

        return foods
    }

}