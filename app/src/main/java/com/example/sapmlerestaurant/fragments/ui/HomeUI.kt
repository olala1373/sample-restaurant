package com.example.sapmlerestaurant.fragments.ui

import android.view.View
import com.example.sapmlerestaurant.fragments.RestaurantFragment
import org.jetbrains.anko.*

class HomeUI : AnkoComponent<RestaurantFragment> {
    companion object HomeFragmentIds{
        val mealFragment = View.generateViewId()
        val foodFragment = View.generateViewId()

    }

    override fun createView(ui: AnkoContext<RestaurantFragment>) =
        with(ui) {
            relativeLayout {
                frameLayout {
                    id = mealFragment
                }.lparams{
                    width = matchParent
                    height = dip(240)
                    centerHorizontally()
                    bottomMargin = dip(8)

                }

                frameLayout {
                    id = foodFragment
                }.lparams{
                    width = matchParent
                    height = matchParent
                    below(mealFragment)
                }
            }
        }
}
