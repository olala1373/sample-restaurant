package com.example.sapmlerestaurant.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sapmlerestaurant.adapters.MealAdapter
import com.example.sapmlerestaurant.fragments.ui.MealUI
import com.example.sapmlerestaurant.models.Food
import com.example.sapmlerestaurant.models.Meal
import com.example.sapmlerestaurant.readJsonFile
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.find
import org.json.JSONArray
import org.json.JSONObject

class MealFragment : Fragment() {

    companion object{
        const val lunchMeal = "Lunch & Dinner"
        const val cuisineType = "cuisineType"
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        MealUI().createView(AnkoContext.create(inflater.context , this , false))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val jsonArray : JSONArray = readJsonFile(context!!)

        val adapter = MealAdapter(extractMealsFromJson(jsonArray))

        val recycler = find<RecyclerView>(MealUI.recyclerId)

        recycler.adapter = adapter
    }

    private fun extractMealsFromJson(jsonArray: JSONArray) : List<Meal>{
        val meals = mutableListOf<Meal>()
        for (i in 0 until jsonArray.length()){
            val mealObject = jsonArray.getJSONObject(i)
            val mealType  = mealObject.getJSONObject("mealType")
            val cuisineObject = mealObject.getJSONObject(cuisineType)
            if (mealType.getString("title") == lunchMeal){
                val image = mealObject.getString("image")
                val title = mealObject.getString("title")
                val subTitle = cuisineObject.getString("title")
                val delivery = mealObject.getString("delivery").toInt()
                val prepration = mealObject.getString("preparation").toInt()
                val rate = mealObject.getInt("rate")
                val time = delivery + prepration
                meals.add(Meal(image , title , subTitle , "$delivery-$time mins" , rate))
            }

        }

        return meals
    }
}