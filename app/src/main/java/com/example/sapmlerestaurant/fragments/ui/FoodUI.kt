package com.example.sapmlerestaurant.fragments.ui

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sapmlerestaurant.fragments.FoodFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class FoodUI : AnkoComponent<FoodFragment> {

    companion object {
        val recyclerId = View.generateViewId()
    }

    override fun createView(ui: AnkoContext<FoodFragment>): View = with(ui){
        relativeLayout{
            recyclerView {
                id = recyclerId
                layoutManager = LinearLayoutManager(context)

            }.lparams{
                width = matchParent
                height = matchParent
                margin = dip(4)
            }
        }
    }
}