package com.example.sapmlerestaurant.fragments.ui

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sapmlerestaurant.fragments.MealFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class MealUI : AnkoComponent<MealFragment> {
    companion object{
        val recyclerId = View.generateViewId()
    }

    override fun createView(ui: AnkoContext<MealFragment>): View = with(ui){
        relativeLayout {
            recyclerView {
                id = recyclerId
                layoutManager = LinearLayoutManager(context , LinearLayoutManager.HORIZONTAL , false)

            }.lparams{
                width = matchParent
                height = matchParent
                margin = dip(4)
            }

        }
    }
}