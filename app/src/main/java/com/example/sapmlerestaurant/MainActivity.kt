package com.example.sapmlerestaurant

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.example.sapmlerestaurant.fragments.RestaurantFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.jetbrains.anko.setContentView

class MainActivity : AppCompatActivity() {

    lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUI().setContentView(this)

        addFragment(RestaurantFragment())
        bottomNavigationView.selectedItemId = R.id.navigation_restaurants

        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    fun addFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(MainActivityUI.fragment_container, fragment)
            .commit()
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        item ->
        when(item.itemId){
            R.id.navigation_restaurants -> {
                Log.v("FUCK RES", "clicked")
                //addFragment(RestaurantFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_bag -> {
                Log.v("FUCK RES", "clicked bag")
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_user -> {
                Log.v("FUCK RES", "clicked user")
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
